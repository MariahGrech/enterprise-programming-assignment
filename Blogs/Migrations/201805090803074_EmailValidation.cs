namespace Blogs.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailValidation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "CreationDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Blogs", "Heading", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Blogs", "Heading", c => c.String(nullable: false));
            DropColumn("dbo.Blogs", "CreationDate");
        }
    }
}
