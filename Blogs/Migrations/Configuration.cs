namespace Blogs.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Blogs.Models; 

    internal sealed class Configuration : DbMigrationsConfiguration<Blogs.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Blogs.Models.ApplicationDbContext";
        }

        protected override void Seed(Blogs.Models.ApplicationDbContext context)
        {
            context.Blogs.AddOrUpdate(b => b.BlogID,
                //c#
                  new Blog()
                  {
                      BlogID = 1,
                      Heading = "Test Heading 1",
                      Category = "C#",
                      Tag = "C#",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 2,
                      Heading = "Test Heading 2",
                      Category = "C#",
                      Tag = "C#",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 3,
                      Heading = "Test Heading 3",
                      Category = "C#",
                      Tag = "C#",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 4,
                      Heading = "Test Heading 4",
                      Category = "C#",
                      Tag = "C#",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 5,
                      Heading = "Test Heading 5",
                      Category = "C#",
                      Tag = "C#",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },

                  //mvc.net
                  new Blog()
                  {
                      BlogID = 6,
                      Heading = "Test Heading 1",
                      Category = "MVC.Net",
                      Tag = "MVC.Net",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 7,
                      Heading = "Test Heading 2",
                      Category = "MVC.Net",
                      Tag = "MVC.Net",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 8,
                      Heading = "Test Heading 3",
                      Category = "MVC.Net",
                      Tag = "MVC.Net",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 9,
                      Heading = "Test Heading 4",
                      Category = "MVC.Net",
                      Tag = "MVC.Net",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  new Blog()
                  {
                      BlogID = 10,
                      Heading = "Test Heading 5",
                      Category = "MVC.Net",
                      Tag = "MVC.Net",
                      Content = "Test",
                      ImagePath = "/Images/War1.png",
                      FileName = null,
                      CreationDate = DateTime.Now
                  },
                  //javascript
                   new Blog()
                   {
                       BlogID = 11,
                       Heading = "Test Heading 1",
                       Category = "JavaScript",
                       Tag = "JavaScript",
                       Content = "Test",
                       ImagePath = "/Images/War1.png",
                       FileName = null,
                       CreationDate = DateTime.Now
                   },

                   new Blog()
                   {
                       BlogID = 12,
                       Heading = "Test Heading 2",
                       Category = "JavaScript",
                       Tag = "JavaScript",
                       Content = "Test",
                       ImagePath = "/Images/War1.png",
                       FileName = null,
                       CreationDate = DateTime.Now
                   },
                   new Blog()
                   {
                       BlogID = 13,
                       Heading = "Test Heading 3",
                       Category = "JavaScript",
                       Tag = "JavaScript",
                       Content = "Test",
                       ImagePath = "/Images/War1.png",
                       FileName = null,
                       CreationDate = DateTime.Now
                   },
                   new Blog()
                   {
                       BlogID = 14,
                       Heading = "Test Heading 4",
                       Category = "JavaScript",
                       Tag = "JavaScript",
                       Content = "Test",
                       ImagePath = "/Images/War1.png",
                       FileName = null,
                       CreationDate = DateTime.Now
                   },
                   new Blog()
                   {
                       BlogID = 15,
                       Heading = "Test Heading 5",
                       Category = "JavaScript",
                       Tag = "JavaScript",
                       Content = "Test",
                       ImagePath = "/Images/War1.png",
                       FileName = null,
                       CreationDate = DateTime.Now
                   },
                   //css
                       new Blog()
                       {
                           BlogID = 16,
                           Heading = "Test Heading 1",
                           Category = "CSS",
                           Tag = "CSS",
                           Content = "Test",
                           ImagePath = "/Images/War1.png",
                           FileName = null,
                           CreationDate = DateTime.Now
                       },
                       new Blog()
                       {
                           BlogID = 17,
                           Heading = "Test Heading 2",
                           Category = "CSS",
                           Tag = "CSS",
                           Content = "Test",
                           ImagePath = "/Images/War1.png",
                           FileName = null,
                           CreationDate = DateTime.Now
                       },
                       new Blog()
                       {
                           BlogID = 18,
                           Heading = "Test Heading 3",
                           Category = "CSS",
                           Tag = "CSS",
                           Content = "Test",
                           ImagePath = "/Images/War1.png",
                           FileName = null,
                           CreationDate = DateTime.Now
                       },
                       new Blog()
                       {
                           BlogID = 19,
                           Heading = "Test Heading 4",
                           Category = "CSS",
                           Tag = "CSS",
                           Content = "Test",
                           ImagePath = "/Images/War1.png",
                           FileName = null,
                           CreationDate = DateTime.Now
                       },
                       new Blog()
                       {
                           BlogID = 20,
                           Heading = "Test Heading 5",
                           Category = "CSS",
                           Tag = "CSS",
                           Content = "Test",
                           ImagePath = "/Images/War1.png",
                           FileName = null,
                           CreationDate = DateTime.Now
                       }
            );

            context.Users.AddOrUpdate(a => a.Id,
            new ApplicationUser()
            {
                BloggerName = "Carmen",
                BloggerSurname = "Abela",
                ProfileDescription = "Test",
                UserName = "CarmenAblela",
                Email = "CarmenAbela@gmail.com"
            },
              new ApplicationUser()
              {
                  BloggerName = "Daniel",
                  BloggerSurname = "Borg",
                  ProfileDescription = "Test",
                  UserName = "DanielBorg",
                  Email = "DanielBorg@gmail.com"
              },
                new ApplicationUser()
                {
                    BloggerName = "Mariah",
                    BloggerSurname = "Magro",
                    ProfileDescription = "Test",
                    UserName = "MariahMagro",
                    Email = "MariahMagro@gmail.com"
                },
                  new ApplicationUser()
                  {
                      BloggerName = "Marisa",
                      BloggerSurname = "Cassar",
                      ProfileDescription = "Test",
                      UserName = "MarisaCassar",
                      Email = "MarisaCassar@gmail.com"
                  },
                    new ApplicationUser()
                    {
                        BloggerName = "Mary",
                        BloggerSurname = "Vella",
                        ProfileDescription = "Test",
                        UserName = "MaryVella",
                        Email = "MaryVella@gmail.com"
                    }
                    ); 
        }
    }
}
