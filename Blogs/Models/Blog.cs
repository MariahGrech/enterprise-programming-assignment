﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blogs.Models
{
    public class Blog
    {
        public int BlogID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Maximum 50 characters")]
        public string Heading { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Tag { get; set; }
        [Required]
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public string FileName { get; set; }//image
        public DateTime CreationDate { get; set; }

        public virtual ApplicationUser User { get; set; }
        //public string ApplicationUserID { get; set; }
    }
}