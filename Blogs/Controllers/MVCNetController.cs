﻿using Blogs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blogs.Controllers
{
    public class MVCNetController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            List<Blog> blogs = db.Blogs.Where(a => a.Category == "MVC.Net").OrderByDescending(a => a.CreationDate).ToList();
            return View(blogs);
        }

        public ActionResult HP()
        {
            List<Blog> blogs = db.Blogs.Where(a => a.Category == "MVC.Net").OrderByDescending(a => a.CreationDate).ToList();
            return View(blogs);
        }
    }
}